//conmment this is my first js code
    console.log('Hello World');
    
    //initialize the variables
    let Name = 'Vyanktesh';
    console.log(Name);
    //rules for intialization of variables
    //Cannot be a reserved keyword
    //should be a meaningful name
    //cannot start with a number
    //cannot contain a space or hyphen
    //are case sensitive
    //default value of the variable is undefined
    let firstName = 'Vyanktesh';
    let lastName = 'Dongre';
    
    // initialize the constants
    const interestRate = 0.3;
    //value of constants cannot be changed

    //initialize boolean
    let isApproved = false;
    //initialize null
    let nothing = null;

    //js is dynamic language
    //values of variables can be changed
    let value = 'String'; // string literal
    value = '1'; // Integer literal
    //value is changed from string literal to Integer literal.
    //This is called as dynamic language

    //Creation of an object
    let name1 = 'Vyanktesh';
    let age1 = 21;
    let person1 = {
        //adding key value pairs
        name1: 'Vyanktesh',
        age1: 21
    };
    //Dot notation 
    person1.name1 = 'Venky';
    //Bracket notation
    person1['name1'] = 'venkatesh';

    console.log(person1);

    //arrays
    let selectedcolors = ['red', 'blue'];
    selectedcolors[2]= 'green';
    console.log(selectedcolors[0]);  //red will be displayed     
    //arrays are dynamic
    selectedcolors[3]= 9;
    console.log(selectedcolors[3]); //array contains string and number so it is dynamic in nature
    console.log(selectedcolors.length); 

    //functions
    function greet(Name) {
        console.log('Hello world!' + Name);
    }

    greet('Vyanktesh'); //calling function greet    
        

